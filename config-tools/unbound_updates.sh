#!/bin/bash
# Updating Unbound resources.
# Place this into e.g. /etc/cron.weekly


###[ root.hints ]###
#curl -o /etc/unbound/root.hints.new https://www.internic.net/domain/named.cache
wget -q -O /etc/unbound/root.hints.new https://www.internic.net/domain/named.cache
if [[ $? -eq 0 ]]; then
  mv /etc/unbound/root.hints /etc/unbound/root.hints.bak
  mv /etc/unbound/root.hints.new /etc/unbound/root.hints
  /usr/sbin/unbound-checkconf >/dev/null
  if [[ $? -eq 0 ]]; then
    rm /etc/unbound/root.hints.bak
    /bin/systemctl reload unbound 
  else
    echo "Warning: Errors in newly downloaded root.hints file probably due to incomplete download:"
    /usr/sbin/unbound-checkconf
    mv /etc/unbound/root.hints /etc/unbound/root.hints.new
    mv /etc/unbound/root.hints.bak /etc/unbound/root.hints
  fi
else
  echo "Download of unbound root.hints failed!"
fi
