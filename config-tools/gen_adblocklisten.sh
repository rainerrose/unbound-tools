#!/bin/bash
WHITELIST="/etc/unbound/unbound.conf.d/Whitelist"
# Verzeichnis, wo sich die Download-Script drin befinden
ADBLOCK_LIST_DOWNLOADER="Adblock_list_downloader"

# Zusaetzlich wird die Datei "whitelist" angelegt
# darin aufgefuehrte Domains (je ein Domain-Name
# pro Zeile) werden grundsaetzlich nich geblockt
# Beispielsweise kann man mit dem Eintrag "buch." verhindern, dass „buch.“ und „polnisch-buch.de“ blockiert werden, nicht aber buchmacher.de.
# Soll nur eine einzelne Domain vom Blocklist-Filter ausgenommen werden, muss der Domain-Name mit doppelten Anführungs­strichen (“) starten und mit einem Punkt (.) enden – zum Beispiel so:
#      "buch.de.

# Dieser Eintrag verhindert, dass die Domain „buch.de“ (und nur diese) geblockt wird. 

ADBLOCK_LIST_DOWNLOADER_DIR=$(dirname "${0}")
# Konfig einlesen
source "${ADBLOCK_LIST_DOWNLOADER_DIR}/unbound-tools.conf"

SCRIPTNAME=$(basename $0 .sh)
ADBLOCK_LIST_DOWNLOADER_DIR="${ADBLOCK_LIST_DOWNLOADER_DIR}/${ADBLOCK_LIST_DOWNLOADER}"


MYTMPDIR=$(mktemp -d -t "unbound-tools-XXXXXXXXXXXXXXXX")
test -e /tmp/dev-adblock || mkdir /tmp/dev-adblock

MYTMPHOSTFILE="${MYTMPDIR}/hosts"
MYTMPHOST_SAMMEL_FILE="${MYTMPDIR}/hosts.sammlung"
OLDIFS=$IFS
IFS="
"

cd "${ADBLOCK_LIST_DOWNLOADER_DIR}"
for i in $(ls -1 *.sh)
do
    #test -z "${i}" && continue
    IFS=$OLDIFS
    # externes Download Script wird aufgerufen
    # Format muss gleich unbound-Syntax entsprechen.
    # Der erwartete Dateiname wird als Parameter übergeben
    ./$i "${MYTMPHOSTFILE}"
    # nur weiter, wenn Script erfolgreich
    if [[ $? -eq 0 ]]; then
        # erstmal alles in einem File sammeln    
        cat "${MYTMPHOSTFILE}" >> "${MYTMPHOST_SAMMEL_FILE}"
    else
        echo "Download-Script $i fehlgeschlagen"
    fi        
done
cd ..
#aufräumen, ggf. download-leichen
test -e "${MYTMPHOSTFILE}" && rm -f "${MYTMPHOSTFILE}"

# Nachbearbeiten
./nachbearbeitung.sh "${MYTMPHOST_SAMMEL_FILE}"

# sortieren, White-List beachten
echo "#Adblocking-Server generiert by ${SCRIPTNAME} um $(date)" > "${BLOCKLIST_UNBOUND_DATEI}.new"
echo "server:" >> "${BLOCKLIST_UNBOUND_DATEI}.new"
test -e "${MYTMPHOST_SAMMEL_FILE}" && sort -u "${MYTMPHOST_SAMMEL_FILE}" | grep -v -f "${WHITELIST}" >> "${BLOCKLIST_UNBOUND_DATEI}.new" && rm "${MYTMPHOST_SAMMEL_FILE}"
# ggf. anlegen
test -e "${BLOCKLIST_UNBOUND_DATEI}" || touch "${BLOCKLIST_UNBOUND_DATEI}"
#sicherung
mv "${BLOCKLIST_UNBOUND_DATEI}" "${BLOCKLIST_UNBOUND_DATEI}.bak"

mv "${BLOCKLIST_UNBOUND_DATEI}.new" "${BLOCKLIST_UNBOUND_DATEI}"

/usr/sbin/unbound-checkconf >/dev/null
if [[ $? -eq 0 ]]; then
    #service unbound reload >/dev/null
    /bin/systemctl reload unbound.service >/dev/null
    rm "${BLOCKLIST_UNBOUND_DATEI}.bak" 
else
    echo "Austausch Adblock-Liste failed"
    mv -v "${BLOCKLIST_UNBOUND_DATEI}" "${BLOCKLIST_UNBOUND_DATEI_FAILED}"
    mv -v "${BLOCKLIST_UNBOUND_DATEI}.bak" "${BLOCKLIST_UNBOUND_DATEI}"
fi

rmdir "${MYTMPDIR}"
