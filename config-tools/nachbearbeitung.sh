#!/bin/bash
DATEI="${1}"
test -z "${DATEI}" && echo "Keine Datei angegeben, zum nachbehandeln" && exit 2
# derzeit kaputt, daher raus
# local-data: "127.0.0.1domain53.com|ameriprise.com|beachbody.com|chick-fil-a.com|citi.com|ebay-kleinanzeigen.de|ebay.at|ebay.be|ebay.ca|ebay.ch|ebay.cn|ebay.co.uk|ebay.com|ebay.com.au|ebay.com.hk|ebay.com.my|ebay.com.sg|ebay.de|ebay.es|ebay.fr|ebay.ie|ebay.it|ebay.nl|ebay.ph|ebay.pl|equifax.ca|equifax.com|globo.com|gumtree.com|lendup.com|mbna.ca|rusneb.ru|sciencedirect.com|sky.com|spectrum.net|td.com|tiaa.org|vedacheck.com|wepay.com|whatleaks.com. 3600 IN A 127.0.0.1"
sed -i '/127.0.0.1domain53.com/d' "${DATEI}"

# Alter Kram als Vorlage
# ist gerade buggy, doppelt drin und Sonderzeichen in FQDN:
# local-data: "d1r90st78epsag.cloudfront.net",. 3600 IN A 127.0.0.1"
# local-data: "d1r90st78epsag.cloudfront.net". 3600 IN A 127.0.0.1"
# also 1) korrigieren
#sed -i 's/local-data: "d1r90st78epsag.cloudfront.net",. 3600 IN A 127.0.0.1"/local-data: "d1r90st78epsag.cloudfront.net. 3600 IN A 127.0.0.1"/' "${DATEI}"
# 2) doppelt, daher raus
# sed -i 's/local-data: "d1r90st78epsag.cloudfront.net". 3600 IN A 127.0.0.1"/ /' "${DATEI}"
#
#eof