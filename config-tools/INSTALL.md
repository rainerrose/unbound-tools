# Installationshinweise

## Voraussetzung für alle Scripte

unbound ist installiert und die optionalen Konfigdateien liegen unterhalb /etc/unbound/unbound.conf.d/ und werden dort alle eingebunden, z.B. via


    include: "/etc/unbound/unbound.conf.d/*.conf"

Weiterhin wird benötigt:

* unzip
* gawk
* wget
* sed
* bash

### Tipp für Debian 10
#### Permission denied
Sofern `unbound` beim Start nörgelt

    Could not open logfile /var/log/unbound.log: Permission denied


Datei `/etc/apparmor.d/local/usr.sbin.unbound` mit folgendem Inhalt erstellen:

~~~
# Site-specific additions and overrides for usr.sbin.unbound.
# For more details, please see /etc/apparmor.d/local/README.
/var/log/unbound.log rw,"
~~~

Danach noch 

~~~ bash
touch /var/log/unbound.log
chown unbound:unbound  /var/log/unbound.log
apparmor_parser -r /etc/apparmor.d/usr.sbin.unbound
~~~

ausführen.

## Logrotate

Datei `/etc/logrotate.d/unbound` anlegen mit folgendem Inhalt

~~~~
/var/log/unbound.log
{
	rotate 7
	daily
	notifempty
	delaycompress
	compress
	postrotate
		 /usr/sbin/unbound-control log_reopen > /dev/null
	endscript
}
~~~~

## Installations-Ort

Die Scripte sind so geschrieben, dass alles unter

    /opt/unbound-tools/

liegen muss.

## config-tools/add2myOwnBlockList4unbound.sh

Ggf. Pfad anpassen:

    ADBLOCK_UNBOUND_DATEI="/etc/unbound/unbound.conf.d/myOwnBlockList.conf"

## config-tools/Adblock_list_downloader

Hier sind die ganzen Download-Scripte der Adblock-Server-Listen versammelt.
Diese werden im Script `gen_adblocklisten.sh` aufgerufen, sofern sie die Endung `.sh` haben.


## gen_adblocklisten.sh

Ggf. Pfade anpassen:

Persönliche Ausnahmeliste:

    WHITELIST="/etc/unbound/unbound.conf.d/Whitelist"

### Persönliche Ausnahmeliste (Whitelist)

Zusaetzlich wird die Datei "whitelist" angelegt darin aufgefuehrte Domains (je ein Domain-Name pro Zeile) werden grundsaetzlich nicht geblockt.

Beispielsweise kann man mit dem Eintrag "buch." verhindern, dass „buch.“ und „polnisch-buch.de“ blockiert werden, nicht aber buchmacher.de.

Soll nur eine einzelne Domain vom Blocklist-Filter ausgenommen werden, muss der Domain-Name mit doppelten Anführungs­strichen (“) starten und mit einem Punkt (.) enden – zum Beispiel so:
    
    "buch.de.

Dieser Eintrag verhindert, dass die Domain „buch.de“ (und nur diese) geblockt wird. 

#### Default-IP

Mit der im c't Artikel erwähnten IP-Adresse kamen die Apple-Geräte nicht klar. Hier half der Tausch zu

    127.0.0.1

Diese IP kann in der Konfig-Datei `unbound-tools.conf` geändert werden.

#### Einrichtung

Projekt clonen mittels git unterhalb von

    /opt 

und einmal wöchentlich ausführen via `crontab` von `root`

    38 6 * * 1 /opt/unbound-tools/gen_adblocklisten.sh 

## unbound_updates.sh

Das Script aktualisiert die Liste der root-DNS-Server.

Irgendwo hinkopieren und wöchentlich aufrufen; z.B. nach `/etc/cron.weekly`

    ln -s -f /opt/unbound-tools/config-tools/unbound_updates.sh /etc/cron.weekly/unbound_updates.sh 


fertig.
