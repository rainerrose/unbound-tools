#!/bin/bash
# Konfig einlesen
ADBLOCK_LIST_DOWNLOADER_DIR=$(dirname "${0}")
source "${ADBLOCK_LIST_DOWNLOADER_DIR}/unbound-tools.conf"
mv "${BLOCKLIST_UNBOUND_DATEI}" "${BLOCKLIST_UNBOUND_DATEI}.bak"
mv "${BLOCKLIST_UNBOUND_DATEI_FAILED}" "${BLOCKLIST_UNBOUND_DATEI}"

/usr/sbin/unbound-checkconf >/dev/null
if [[ $? -eq 0 ]]; then
    /bin/systemctl reload unbound.service >/dev/null
    rm "${BLOCKLIST_UNBOUND_DATEI}.bak" 
else
    echo "Austausch Adblock-Liste failed"
    mv -v "${BLOCKLIST_UNBOUND_DATEI}" "${BLOCKLIST_UNBOUND_DATEI_FAILED}"
    mv -v "${BLOCKLIST_UNBOUND_DATEI}.bak" "${BLOCKLIST_UNBOUND_DATEI}"
fi