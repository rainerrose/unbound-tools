#!/bin/bash
# add2myOwnBlockList4unbound.sh
# Skript zum hinzufügen einer Domain zur Adblocking-Konfiguration in unbound
ADBLOCK_UNBOUND_DATEI="/etc/unbound/unbound.conf.d/myOwnBlockList.conf"
DOMAIN_TO_BLOCK="$1"

test -z $DOMAIN_TO_BLOCK && echo "usage: $0 <domain>" && exit

# Konfig einlesen
source "unbound-tools.conf"

cp "${ADBLOCK_UNBOUND_DATEI}" "${ADBLOCK_UNBOUND_DATEI}.bak"
echo "  local-data: \""${DOMAIN_TO_BLOCK}". 3600 IN A ${REDIRECT_TARGET}\"" >> "${ADBLOCK_UNBOUND_DATEI}"
/usr/sbin/unbound-checkconf
if [[ $? -eq 0 ]]; then
	/bin/systemctl reload unbound.service >/dev/null
	rm "${ADBLOCK_UNBOUND_DATEI}.bak" 
else
	echo "Block-Liste failed"
	mv -v "${ADBLOCK_UNBOUND_DATEI}" "${ADBLOCK_UNBOUND_DATEI}.failed"
	mv -v "${ADBLOCK_UNBOUND_DATEI}.bak" "${ADBLOCK_UNBOUND_DATEI}"
fi
