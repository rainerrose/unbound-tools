#!/bin/bash
# Dateiname wird übergeben, wo reingeschrieben werden soll.
# Skript zum Erzeugen einer Blocking-Konfiguration gegen CryptoMiner
ADBLOCK_ERGEBNIS_DATEI="${1}"

HOSTURL="https://raw.githubusercontent.com/hoshsadiq/adblock-nocoin-list/master/hosts.txt"

SCRIPTNAME=$(basename $0 .sh)
EXIT_SUCCESS=0
EXIT_FAILURE=1
EXIT_ERROR=2
EXIT_BUG=10
EXIT_CODE=$EXIT_ERROR

test -z "${REDIRECT_TARGET}" && REDIRECT_TARGET="127.0.0.1"

MYTMPDIR=$(mktemp -d -t "${SCRIPTNAME}-XXXXXXXXXXXXXXXX")
cd "${MYTMPDIR}"
wget -q -O  hosts "${HOSTURL}"
if [[ $? -eq 0 ]]; then
    grep -v "^127.0"  hosts| grep -v "^#"| grep -v "^$" |\
    awk -v redirect=$REDIRECT_TARGET '{if ($2!="0.0.0.0") print "  local-data: \""$2". 3600 IN A "redirect"\""}'\
    > "${ADBLOCK_ERGEBNIS_DATEI}"
    EXIT_CODE=$EXIT_SUCCESS
else
	echo "Download of unbound nocoin-adblock-liste failed!"
    EXIT_CODE=$EXIT_FAILURE
fi
test -e hosts && rm hosts
cd
rmdir "${MYTMPDIR}"
exit $EXIT_CODE
