# unbound-tools
## Zweck
Die Idee zu diesen Tools entstammt einer Artikelserie der Zeitschrift c't vom heise-Verlag zum DNS-resolver unbound. Die Artikel sind am Ende dieses Dokuments aufgelistet.

Die dort erwähnten Scripte sind nur rudimentär, daher habe ich sie aufgebohrt und noch ein paar hinzugefügt:

* automatisches aktualisieren der Adblock-Liste
* eigene Domains in die Blockliste aufnehmen
* DNS-root-Server updaten
* kleine Weboberflächen
   * Adblock-Liste zu aktivieren und deaktivieren
   * Logging der Anfragen
       * zu aktivieren und deaktivieren
       * anzuzeigen und zu durchsuchen

## System-Voraussetzungen

Getestet unter


|Distribution            |unbound Version  |Hinweise                                                                       |
|------------------------|-----------------|-------------------------------------------------------------------------------|
|Debian 9.3 (stretch)    | 1.6.0-3  | `systemctl`-Aufrufe müssen dann in `service` in sämtlichen Scripten umgebaut werden! |
|Debian 11.4 (bullseye)  | 1.13.1   | aktuelle Version bzw. ab Repo-Tag: v.2.6     |

 

## Installation

Es exisitieren jeweils Installationshinweise in folgenden Dateien

* [`config-tool/INSTALL.md`](config-tool/INSTALL.md)
* [`website/INSTALL.md`](website/INSTALL.md)

## Tools
### Config-Tools
#### unbound-tools.conf

Zur Redundanz-Vermeidung werden hier Umgebungsvariablen gesetzt. Hier kann auch die Default-IP gesetzt werden,
falls es Umstände zur Änderungen geben sollte. Apple-Geräte hatten mit der im c't-Artikel genannten IP Probleme.
Diese IP kann jetzt hier gesetzt werden (ab Version 2.3)

#### add2myOwnBlockList4unbound.sh

Hiermit können eigene Domains auf den Index bzw. die Blocklist gesetzt werden.

### gen_adblocklisten.sh

Das Script aktualisiert die Blockliste der üblich-verdächtigen Werbenetzwerke. Es können mehrere Listen unterschiedlicher Projekte angezogen werden.

Dieses ist das zentrale Script, welches die Scripte aus dem Unterverzeichnis `Adblock_list_downloader` einliest, sofern sie die Endung `.sh` haben.

### failliste_nochmal_einspielen.sh

Manchmal schleichen sich bei einigen Listen Fehler ein. Das Script speichert dann den Versuch als
`adblock.conf.fail`. Diese Datei kann nachbearbeitet werden und dann mit Hilfe dieses Scriptes nocheinmal
versucht werden einzuspielen.

#### nachbearbeitung.sh

Falls sich dauerhaft Fehler einschleichen, findet hier zentral eine Nachbehandlung statt, d.h. man muss nicht wissen, in welcher Liste bzw. welchem Downloader sich ein Fehler eingeschlichen hat.

Cronjob-Beispiel-Output


    1670222894] unbound-checkconf[1008985:0] error: error parsing local-data at 256 '127.0.0.1domain53.com|amer

Die zusammengestellte Datei wird als Parameter übergeben und kann dann z.B. mit `sed` behandelt werden.

Zum Beispiel:

~~~ bash
sed -i '/127.0.0.1domain53.com/d' "${DATEI}"
~~~

damit die Zeile rausfliegt.
Würde sie drin bleiben, gäbe es einen Syntax-Fehler beim Checken der Konfig und die alte  Blockliste würde bestehen bleiben und keine Aktualisierung erfahren.


#### unbound_updates.sh

Das Script aktualisiert die Liste der root-DNS-Server.

### Download-Scripte

##### GoodbyeAds.sh

Liste von Jerry Joseph und seinem GoodbyeAds auf Github unter https://github.com/jerryn70/GoodbyeAds .

##### moab2unbound.sh

Das Script aktualisiert die Blockliste der üblich-verdächtigen Werbenetzwerke aus einem Thread des Forums xda-developers. Scheint derzeit (Oktober 2019) nicht gepflegt zu werden, Script ist daher deaktiviert.

##### nocoin2unbound.sh

Mit diesem Script pflegt die Blocklist bekannter Crypto-Mining-IPs. Grundlage ist der c't-Artikel Heft 09/2018, S. 84, basierend auf dem host-File des Projektes [adblock-nocoin-list](https://github.com/hoshsadiq/adblock-nocoin-list) auf github.

##### someonewhocares.org.sh

Grundlage ist die Hosts-Datei die dan pollock unter https://someonewhocares.org/hosts/ bereitstellt.

#### StevenBlack.sh

Grundlage ist die Hosts-Datei die dan pollock unter https://github.com/StevenBlack/hosts bereitstellt.

##### VORLAGE.sh.skel

Dieses Script kann als Vorlage für neue Listen dienen. Beim umkopieren in ein neues Script muss die Endung `.skel` entfernt werden.

### Weboberfläche

Enthält eine kleine Weboberfläche im responsive Design, mit dem man die Adblock-Listen aktivieren und deaktivieren kann. Manchmal möchte man den "Adblocker" ja doch ausschalten, um z.B. Werbeprämien in Onlineshops sich via Cashback oder ähnliches vergüten zu lassen.
Ein kleines Script, zum Ein- und Ausschalten liegt ebenfalls dabei.

### Logging

Es gibt zwei Oberflächen

1. anzeigen des Logs 
2. logging der Anfragen ein- oder ausschalten

Zum Auswerten der Logs habe ich mich des Projektes https://github.com/taktos/php-tail bedient und ein paar Zeilen verändert.

## Hinweis

Viel Spaß beim Benutzen.

Als Lizenz findet die GNU GPL v2 Anwendung.

## c't Artikel
* [c't 21/2017, S. 158](https://www.heise.de/ct/ausgabe/2017-21-Privater-Nameserver-und-Adblocker-im-LAN-3837632.html) 
* [c't 04/2018, S. 162](https://www.heise.de/ct/ausgabe/2018-4-Update-zu-Unbound-als-privater-Nameserver-und-Adblocker-3951756.html)
* [c't 09/2018, S. 84](https://www.heise.de/ct/ausgabe/2018-9-Mining-Parasiten-erkennen-und-loswerden-4013384.html?wt_mc=print.ct.2018.09.84#zsdb-article-links)

# Know Bugs
## gen_adblocklisten.sh
Lässt sich derzeit nur mit vollem, absoluten Pfad aufrufen; siehe Issue #8