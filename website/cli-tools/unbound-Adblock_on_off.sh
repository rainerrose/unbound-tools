#!/bin/bash
#Schaltet die Adblocking-Konfiguration (MOAB) ein bzw aus
ADBLOCK_UNBOUND_DATEI="/etc/unbound/unbound.conf.d/adblock.conf"
STATUS="${1}"
test -z "${STATUS}" && echo "usage: $0 [ein|aus]" && exit 2

RUECK=2

if [ "${STATUS}" == "ein" ]
then
    test -e "${ADBLOCK_UNBOUND_DATEI}.deaktiviert" && mv "${ADBLOCK_UNBOUND_DATEI}.deaktiviert" "${ADBLOCK_UNBOUND_DATEI}" 
    /bin/systemctl reload unbound >/dev/null
    RUECK=0
elif [ "${STATUS}" == "aus" ]
then
    test -e "${ADBLOCK_UNBOUND_DATEI}" && mv "${ADBLOCK_UNBOUND_DATEI}" "${ADBLOCK_UNBOUND_DATEI}.deaktiviert"
    /bin/systemctl reload unbound >/dev/null
    RUECK=0
else
    echo "falscher Status"
    RUECK=2
fi
exit $RUECK
