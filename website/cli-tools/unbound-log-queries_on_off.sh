#!/bin/bash
#Schaltet die loggen der Queries ein bzw aus
CONF_DATEI="/etc/unbound/unbound.conf.d/01_CacheForwarder.conf"
STATUS="${1}"
test -z "${STATUS}" && echo "usage: $0 [ein|aus]" && exit 2

RUECK=2

if [ "${STATUS}" == "ein" ]
then
    sed -i 's/log-queries: no/log-queries: yes/' "${CONF_DATEI}"
    /usr/sbin/unbound-checkconf >/dev/null
    if [[ $? -eq 0 ]]; then
        /bin/systemctl reload unbound >/dev/null
        RUECK=0
    else
        echo "Syntax-Fehler in unbount"
        RUECK=1
    fi
elif [ "${STATUS}" == "aus" ]
then
    sed -i 's/log-queries: yes/log-queries: no/' "${CONF_DATEI}"
    if [[ $? -eq 0 ]]; then
        /bin/systemctl reload unbound >/dev/null
        RUECK=0
    else
        echo "Syntax-Fehler in unbount"
        RUECK=1
    fi
else
    echo "falscher Status"
    RUECK=2
fi
exit $RUECK
