# Weboberfläche - Installation

Machmal möchte man die Blocklist mal kurzzeitig deaktivieren. Dazu gibt es ein kleines Bash-Script was genau das tut, sowie eine kleine Weboberfläche mit genau zwei Knöppen: Ein und aus.

Das Bash-Script wird vom Webserver angestoßen und benennt einfach die Blocklist um und startet den Unbound-Daemon durch.

## Voraussetungen

Der unbound-Server ist installiert und auf dem selben Server läuft auch ein Webserver mit einem halbwegs aktuellen PHP. Es dürfte wahrscheinlich auch noch unter PHP4 funktionieren, habe ich aber nicht ausprobiert. Aktuell läuft bei mir ein PHP5.

Sofern noch AppArmor oder SELinux läuft müssen hier ggf. noch Einstellungen vorgenommen werden, die aber nicht Teil dieser Doku sind.

## Bash-Script

### Installation

Das Bash-Script aus

    cli-tools/unbound-Adblock_on_off.sh

kopieren nach

    /opt/unbound-tools/website/cli-tools/unbound-Adblock_on_off.sh

und danach dem Webserver per sudo-Recht das Aufrufen desselbigen erlauben. Also per

    visudo

als Systembenutzer oder `root` die Datei aufmachen und folgende Zeilen hinzufügen

    www-data     ALL = NOPASSWD: /opt/unbound-tools/website/cli-tools/unbound-Adblock_on_off.sh ein, /opt/unbound-tools/website/cli-tools/unbound-Adblock_on_off.sh aus 
    www-data     ALL = NOPASSWD: /opt/unbound-tools/website/cli-tools/unbound-log-queries_on_off.sh ein, /opt/unbound-tools/website/cli-tools/unbound-log-queries_on_off.sh aus 

Der Benutzer `www-data` ist ggf. an die lokale Gegebenheiten anzupassen. Je nachdem unter welchem Benutzer der Webserver läuft. Manchmal ist es auch z.B. `httpd` .

## Weboberfläche

Die `php`-Dateien aus dem Unterverzeichnis `webpage` hinkopieren. Sinn macht sicher ein leicht zu merkender Verzeichnisname, z.B. `dns`.

Die PHP-Script nutzt die Bibilotheken von JQuery und MetroUI. Da ich mich gerade mit den Lizenzen nicht auskenne, packe ich sie erstmal nicht in dieses Repo.

## jQuery

jQuery wird in der Version 3.2.1 benötigt.

Runterladen und unterhalb des Scriptes in das Verzeichnis `js` kopieren als 

    js/jquery.js

## Metro UI 

Wird in der Version 3 benötigt und zwar wieder verteilt in Unterverzeichnisse mit folgenden Dateien:

    css/metro.min.css
    css/metro-icons.min.css
    css/metro-responsive.min.css
    js/metro.min.js

Metro UI in der Version 3 ist runterladbar auf: https://github.com/olton/Metro-UI-CSS/tree/v3.0.18/build

Die API-Beschreibung findet sich unter: https://metroui.org.ua/v3/

## Fertig

Jetzt sollte die "Webseite" erreichbar sein und dort sollte ein roter "Ausschalten"- und ein grüner "Einschalten"-Button sein, sowie der aktuelle Zustand.

Beispiel-Screenshot siehe:

![Webseiten-Screeeshot](docs/webseiten-screenshot.jpg)



