<?php
/**
 * Require the library
 */
require 'PHPTail.php';
/**
 * Initilize a new instance of PHPTail
 * @var PHPTail
 */

$tail = new PHPTail(array(
    "Unbound_Log" => "/var/log/unbound.log",
    "Unbound_Conf" => "/etc/unbound/unbound.conf.d/01_CacheForwarder.conf",
#    "AdBlock_Liste" => "/etc/unbound/unbound.conf.d/adblock.conf",
));

/**
 * We're getting an AJAX call
 */
if(isset($_GET['ajax']))  {
    echo $tail->getNewLines($_GET['file'], $_GET['lastsize'], $_GET['grep'], $_GET['invert']);
    die();
}

/**
 * Regular GET/POST call, print out the GUI
 */
$tail->generateGUI();
