<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <link href="css/metro.min.css" rel="stylesheet">
    <link href="css/metro-icons.min.css" rel="stylesheet">
    <link href="css/metro-responsive.min.css" rel="stylesheet">
    <script src="js/jquery.js"></script>
    <script src="js/metro.min.js"></script>
</head>
<body>
<p>
<?php

$status="";
if (isset($_REQUEST['status']))
{
	$status=$_REQUEST['status'];
	if ($status == "ein")
	{
        system("sudo /opt/unbound-tools/website/cli-tools/unbound-Adblock_on_off.sh ein");
    }
    elseif ($status == "aus")
    {
        system("sudo /opt/unbound-tools/website/cli-tools/unbound-Adblock_on_off.sh aus");
    }
}
?>

Aktueller Status:&nbsp;

<?php
if (file_exists("/etc/unbound/unbound.conf.d/adblock.conf"))
{
	echo '<span class="tag success">Anti-Spy an</span>';
}
else
{
	echo '<span class="tag alert">Anti-Spy aus</span>';
}
?>

<a href="?">Seite neuladen <span class="mif-loop2"></span></a>
</p>

<pre>
sudo systemd-resolve --flush-caches
sudo resolvectl flush-caches
ipconfig /flushdns
</pre>

<p>
<table class="table">
<tr>
	<td>
			<form>
			<input type="hidden" name="status" value="ein">
			<button type = "submit" class="button success">Einschalten</button>
			</form>
	</td>
	<td>
			<form>
			<input type="hidden" name="status" value="aus">
			<button type = "submit" class="button danger">Ausschalten</button>
			</form>
	</td>
</tr>
</table>
</p>
<ul>
<li><a href="Log.php">Logfile anzeigen <span class="mif-file-text"></span><span class=" mif-search"></span></a></li>
<li><a href="switch_logging.php">Logging der Queries ein-/ausschalten <span class="mif-tools"></span></a></li>
</ul>
<p>
</p>
</body>
</html>

